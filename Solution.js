const fs = require("fs");
const path = require('path')

const onlineShopping = {
    id: Math.floor(Math.random() * 1000000000),

    generateLoginId(username, type = 0) {
        return new Promise((resolve, reject) => {
            if (username) {
                if (username.length > 8) {
                    return reject({code: 400, reason: "BadRequest"})
                } else {
                    if (type === 1) {
                        return resolve({'Id': this.id, 'Type': 'Admin'})
                    } else {
                        return resolve({'Id': this.id, 'Type': 'Guest'})
                    }
                }
            } else {
                reject({code: 400, reason: "BadRequest"})
            }
        })
    },
    addItem(items) {
        return new Promise((resolve, reject) => {
            fs.readFile(path.join(__dirname, 'cart.json'), 'utf8', (error, _items) => {
                if (error) {
                    reject({code: 400, reason: "BadRequest"})
                } else {
                    let cart = JSON.parse(_items)
                    fs.readFile(path.join(__dirname, 'products.json'), 'utf8', (err, data) => {
                        if (err) {
                            reject({code: 400, reason: "BadRequest"})
                        } else {
                            let productList = JSON.parse(data)
                            if (Array.isArray(items)) {
                                for (let item; item < items.length; item++) {
                                    for (let productId in productList) {
                                        if (productList[productId].name === item) {
                                            if (cart[this.id]) {
                                                let itemExist = false
                                                let index
                                                for (let i; i < cart[this.id].length; i++) {
                                                    if (cart[this.id][i]._id === productId) {
                                                        itemExist = true
                                                        index = i
                                                        return
                                                    }
                                                }
                                                if (itemExist) {
                                                    cart[this.id][index].quantity++
                                                } else {
                                                    cart[this.id].push({
                                                        "_id": productList[productId],
                                                        "name": item,
                                                        "quantity": 1
                                                    })
                                                }
                                            } else {
                                                cart[this.id] = [{
                                                    "_id": productId,
                                                    "name": item,
                                                    "quantity": 1
                                                }]
                                            }
                                        }
                                    }
                                }
                            } else {
                                for (let productId in productList) {
                                    if (productList[productId].name === item) {
                                        if (cart[this.id]) {
                                            let itemExist = false
                                            let index
                                            for (let i; i < cart[this.id].length; i++) {
                                                if (cart[this.id][i]._id === productId) {
                                                    itemExist = true
                                                    index = i
                                                    return
                                                }
                                            }
                                            if (itemExist) {
                                                cart[this.id][index].quantity++
                                            } else {
                                                cart[this.id].push({
                                                    "_id": productList[productId],
                                                    "name": item,
                                                    "quantity": 1
                                                })
                                            }
                                        } else {
                                            cart[this.id] = [{
                                                "_id": productId,
                                                "name": item,
                                                "quantity": 1
                                            }]
                                        }
                                    }
                                }
                            }
                            let filesToWrite = JSON.stringify(cart)
                            fs.writeFile(path.join(__dirname, 'cart.json'), filesToWrite, (error1, _) => {
                                if (error1) {
                                    reject({code: 400, reason: "BadRequest"})
                                }
                            })
                        }
                    })
                }
            })
        })
    },
    costliestItem() {
        return new Promise((resolve, reject) => {
            fs.readFile(path.join(__dirname, 'price.json'), 'utf8', (error, prices) => {
                if (error) {
                    reject({code: 400, reason: "BadRequest"})
                } else {
                    let priceList = JSON.parse(prices)
                    let mostCostly = 0
                    let mostCostlyId
                    for (let priceId in priceList) {
                        let cost = priceList[priceId].price - priceList[priceId].discount
                        if (cost > mostCostly) {
                            mostCostlyId = priceId
                            mostCostly = cost
                        }
                    }
                    fs.readFile(path.join(__dirname, 'cart.json'), 'utf8', (error, items) => {
                        if (error) {
                            reject({code: 400, reason: "BadRequest"})
                        } else {
                            let cart = JSON.parse(items)
                            fs.readFile(path.join(__dirname, 'products.json'), 'utf8', (err, data) => {
                                if (err) {
                                    reject({code: 400, reason: "BadRequest"})
                                } else {
                                    let productList = JSON.parse(data)
                                    for (let productId in productList) {
                                        if (productId === mostCostlyId) {
                                            if (cart[this.id]) {
                                                let itemExist = false
                                                let index
                                                for (let i; i < cart[this.id].length; i++) {
                                                    if (cart[this.id][i]._id === productId) {
                                                        itemExist = true
                                                        index = i
                                                        return
                                                    }
                                                }
                                                if (itemExist) {
                                                    cart[this.id][index].quantity++
                                                } else {
                                                    cart[this.id].push({
                                                        "_id": productList[productId],
                                                        "name": productList[productId].name,
                                                        "quantity": 1
                                                    })
                                                }
                                            } else {
                                                cart[this.id] = [{
                                                    "_id": productId,
                                                    "name": productList[productId].name,
                                                    "quantity": 1
                                                }]
                                            }
                                        }
                                    }
                                    let filesToWrite = JSON.stringify(cart)
                                    fs.writeFile(path.join(__dirname, 'cart.json'), filesToWrite, (error1, _) => {
                                        if (error1) {
                                            reject({code: 400, reason: "BadRequest"})
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        })
    },
    getProductInformation() {
        return new Promise((resolve, reject) => {
            fs.readFile(path.join(__dirname, 'price.json'), 'utf8', (error, prices) => {
                if (error) {
                    reject({code: 400, reason: "BadRequest"})
                } else {
                    let priceList = JSON.parse(prices)
                    fs.readFile(path.join(__dirname, 'products.json'), 'utf8', (err, data) => {
                        if (err) {
                            reject({code: 400, reason: "BadRequest"})
                        } else {
                            let productList = JSON.parse(data)

                            for (let productId in productList) {
                                let productInformation = {
                                    id: productId,
                                    "name": productList[productId].name,
                                    "priceInformation": priceList[productId]
                                }
                                fs.appendFile(path.join(__dirname, 'Product_Information.json'), JSON.stringify(productInformation) + '\n' + ',', (err, _) => {
                                    if (err) {
                                        reject({code: 400, reason: "BadRequest"})
                                    }
                                })
                            }

                        }
                    })
                }
            })
        })
    },
    updateCatalogue(loginType, itemsToAdd, pricesToAdd) {
        return new Promise((resolve, reject) => {
            if (loginType === 0) {
                return reject({code: 400, reason: "BadRequest"})
            } else {
                fs.readFile(path.join(__dirname, 'products.json'), 'utf8', (err, data) => {
                    if (err) {
                        reject({code: 400, reason: "BadRequest"})
                    } else {
                        let productList = JSON.parse(data)
                        if (Array.isArray(itemsToAdd)) {
                            productList = Object.assign(productList, itemsToAdd[0])
                            fs.writeFile(path.join(__dirname, 'products.json'), productList, (error1, _) => {
                                if (error1) {
                                    reject({code: 400, reason: "BadRequest"})
                                }
                            })
                        } else {
                            productList = Object.assign(productList, itemsToAdd)
                            fs.writeFile(path.join(__dirname, 'products.json'), productList, (error1, _) => {
                                if (error1) {
                                    reject({code: 400, reason: "BadRequest"})
                                }
                            })
                        }
                    }
                })
                fs.readFile(path.join(__dirname, 'price.json'), 'utf8', (error, prices) => {
                    if (error) {
                        reject({code: 400, reason: "BadRequest"})
                    } else {
                        let priceList = JSON.parse(prices)
                        if (Array.isArray(pricesToAdd)) {
                            priceList = Object.assign(priceList, pricesToAdd[0])
                            fs.writeFile(path.join(__dirname, 'price.json'), priceList, (error1, _) => {
                                if (error1) {
                                    reject({code: 400, reason: "BadRequest"})
                                }
                            })
                        } else {
                            priceList = Object.assign(priceList, pricesToAdd)
                            fs.writeFile(path.join(__dirname, 'price.json'), priceList, (error1, _) => {
                                if (error1) {
                                    reject({code: 400, reason: "BadRequest"})
                                }
                            })
                        }
                    }
                })
            }
        })
    }
}




